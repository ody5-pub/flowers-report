# Flowers Project Report

These are the report files for the flowers project.

# License
[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)  
The text is released under the [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/), and code is released under the [MIT license](https://opensource.org/licenses/MIT).
